#!/bin/bash
set -xe

export BSP_VER=${BSP_VER:-"35.4.1"}
export L4T_VER=${L4T_VER:-"l4t/l4t-r35.4.ga"}
export OUT=$1

clone_mod() {
	if [[ ! -e $2 ]]; then
		[[ -z $OUT ]] && OUT=$2
		if [[ $1 == "linux-5.10" ]]; then
			git submodule add -b ${L4T_VER}-5.10 https://nv-tegra.nvidia.com/r/$1 $2
		else
			git submodule add -b ${L4T_VER} https://nv-tegra.nvidia.com/r/$1 $2
		fi
	fi
}

kernel() {
	mkdir -p kernel
	pushd kernel

	git init
	git checkout -b ${BSP_VER}
	git remote add l4t git@gitlab.azka.li:l4t-community/kernel/t194/kernel

	clone_mod linux-5.10 kernel-5.10
	git -C kernel-5.10 remote add l4t https://gitlab.azka.li/l4t-community/kernel/t194/kernel-5.10
	git -C kernel-5.10 fetch l4t
	git -C kernel-5.10 cherry-pick 6a42b7197b93db8e7e25b2161c57826618f8e583^...74fa9247570d720a7fe55be65a7ea45b038b997e
	git -C kernel-5.10 checkout -b jetson_${BSP_VER}_ax210

	clone_mod linux-nvgpu nvgpu
	clone_mod linux-nvidia nvidia
	clone_mod kernel/nvethernetrm nvethernetrm
	popd
}

plat() {
	mkdir -p hardware/nvidia/platform
	pushd hardware/nvidia/platform

	git init
	git checkout -b ${BSP_VER}
	git remote add l4t git@gitlab.azka.li:l4t-community/kernel/t194/hardware/nvidia/platform

	clone_mod device/hardware/nvidia/platform/t19x/common t19x/common
	clone_mod device/hardware/nvidia/platform/t19x/stardust-dts t19x/galen/kernel-dts
	clone_mod device/hardware/nvidia/platform/t19x/galen-industrial-dts t19x/galen-industrial/kernel-dts
	clone_mod device/hardware/nvidia/platform/t19x/jakku-dts t19x/jakku/kernel-dts

	clone_mod device/hardware/nvidia/platform/t23x/common-dts t23x/common/kernel-dts
	clone_mod device/hardware/nvidia/platform/t23x/concord-dts t23x/concord/kernel-dts
	clone_mod device/hardware/nvidia/platform/t23x/p3768-dts t23x/p3768/kernel-dts

	clone_mod device/hardware/nvidia/platform/tegra/common tegra/common

	popd
}

soc() {
	mkdir -p hardware/nvidia/soc
	pushd hardware/nvidia/soc

	git init
	git checkout -b ${BSP_VER}
	git remote add l4t git@gitlab.azka.li:l4t-community/kernel/t194/hardware/nvidia/soc

	clone_mod device/hardware/nvidia/soc/t19x t19x
	clone_mod device/hardware/nvidia/soc/t23x t23x
	clone_mod device/hardware/nvidia/soc/tegra tegra

	popd
}

mkdir -p $OUT
pushd $OUT
kernel
plat
soc
popd
