#!/bin/bash
mkdir -p out/gcc/

export CWD=$(realpath .)
export BSP_VER=${BSP_VER:-"35.4.1"}
export CROSS_COMPILE_AARCH64_PATH=$(realpath ${CWD}/out/gcc)

if [[ ! -e "${CROSS_COMPILE_AARCH64_PATH}/bin" ]]; then
	wget https://developer.nvidia.com/embedded/jetson-linux/bootlin-toolchain-gcc-93 -O  ${CROSS_COMPILE_AARCH64_PATH}/gcc-9.3.tbz2
	tar xf ${CROSS_COMPILE_AARCH64_PATH}/gcc-9.3.tbz2 -C ${CROSS_COMPILE_AARCH64_PATH}
	rm ${CROSS_COMPILE_AARCH64_PATH}/gcc-9.3.tbz2
fi

clone() {
	if [[ ! -e ${CWD}/out/$2 ]]; then
		git clone --recurse-submodules -j$(nproc) -b ${BSP_VER} https://gitlab.azka.li/l4t-community/kernel/t194/$1 ${CWD}/out/$2
	fi
}

mkdir -p ${CWD}/out/hardware/nvidia/
if [[ $1 == "repo" ]]; then
	clone kernel kernel
	clone hardware/nvidia/soc hardware/nvidia/soc
	clone hardware/nvidia/platform hardware/nvidia/platform
else
	cd ${CWD}/out
	${CWD}/submodules.sh ${CWD}/out/
	cd ${CWD}
fi

# HACK: Symlink nvethernetm
ln -sfn ../../../../../../nvethernetrm/ ${CWD}/out/kernel/nvidia/drivers/net/ethernet/nvidia/nvethernet/nvethernetrm

rm -rf out/files/
mkdir -p out/files/boot/dtb/
${CWD}/nvbuild.sh -o ${CWD}/out/build
